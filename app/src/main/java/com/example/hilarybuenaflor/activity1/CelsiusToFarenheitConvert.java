package com.example.hilarybuenaflor.activity1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
public class CelsiusToFarenheitConvert extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_celsius_to_farenheit_convert);
    }

    public void OnButtonClick(View v) {
            TextView ResultOutputTxt2 = (TextView)findViewById(R.id.ResultOutputTxt);
            EditText celtxt = (EditText)findViewById(R.id.CelsiusText);
            double celsiusT = Double.parseDouble(celtxt.getText().toString());
            //Formula
            double ans = (celsiusT * 9/5) + 32;
            ResultOutputTxt2.setText(Double.toString(ans) + "°F");
        }
}
